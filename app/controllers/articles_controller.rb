class ArticlesController < ApplicationController
  after_action :destroy_sesstion, only: [:index]
  def index
    @session = session
    @articles = Article.all
  end

  def show
    @article = Article.find_by(id: params[:id])
    session[:datasum] = @article
  end

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
 end


private
  def article_params
    params.require(:article).permit(:title, :text )
  end
def destroy_sesstion
  session[:datasum] = nil
end

end
